<?php
/**
 * Template Name: Partner Carousel
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php
/*
*  Create the Markup for a slider
*  This example will create the Markup for Flexslider (http://www.woothemes.com/flexslider/)
*/
$logos = get_field('logo_carousel');
if( $logos ) { ?>

<div id="slider" class="slick-slider">
		<?php foreach( $logos as $logo ): ?>
			<div>
				<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
			</div>
		<?php endforeach; ?>
</div>

<?php } ?>
<!-- End Photo Slider -->

</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
var $f = jQuery.noConflict(true);
  $f(window).load(function() {
    $f('.slick-slider').slick(
      {
        centerMode: true,
         centerPadding: '60px',
         slidesToShow: 4,
         autoplay: true,
autoplaySpeed: 5000,
         responsive: [
           {
             breakpoint: 768,
             settings: {
               arrows: true,
               centerMode: true,
               centerPadding: '40px',
               slidesToShow: 2
             }
           }
         ]
    }
  );
  });
</script>
