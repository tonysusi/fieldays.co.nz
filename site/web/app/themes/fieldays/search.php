<?php get_template_part('templates/page', 'header'); ?>
<h4 class="title">Search results for <span class="search-results"><?php the_search_query(); ?></span></h4>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php
  $content = get_search_form(false);
  var_dump($content);
  ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'search'); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
