(function($) {

  var backToTop = {
    init: function() {
      // find the height of the viewport and the container
      var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      var div = document.querySelectorAll('div.wrap.container');
      var divH = div[0].clientHeight;

      //if div height is larger than viewport, add back to top button
      if (divH > h) {
        backToTop.add();
      }
    },
    add: function() {
      // Add to main.main
      $('footer.content-info').prepend('<div class="backToTop__block"><div id="backToTop" class="backToTop__button"><span class="backToTop__line"></span>BACK TO TOP</div></div>');

      // set to show backToTop button if scrolled 100px
      // ===== Scroll to Top ====
      $(window).scroll(function() {
        if ($(this).scrollTop() >= 150) { // If page is scrolled more than 50px
          $('#backToTop').fadeIn(200); // Fade in the arrow
        } else {
          $('#backToTop').fadeOut(200); // Else fade out the arrow
        }
      });

      //Scroll to top
      $('#backToTop').click(function() {
        $('html, body').animate({
          scrollTop: $('main.main').offset().top
        }, 500);
      });

    }
  };

  // start backToTop
  backToTop.init();

  var search = {
    init: function() {

      // $('#menu-primary-menu').append('<form role="search" method="get" class="search-form" action=""><input type="submit" class="search-submit" value="Search" /></form>');

      // $('#menu-item-search').on('click touchstart', function() {
      //   $('#full-screen-search').addClass('open');
      //   console.log('open search');
      // });

      // console.log('url:',window.location.href );

      search.addMenu();
      search.addModal();

      var timer;

      timer = setInterval(function() {
        search.actions();
      }, 500);

    },
    addMenu: function() {
      // add Search to primary menu
      $('#menu-primary-menu').append('<li id="menu-item-search" class="menu-item"><a href="">Search</a></li>');
    },
    addModal: function() {
      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";

      var modal = '<div id="full-screen-search" class=""><button type="button" class="close" id="full-screen-search-close">X</button><form role="search" method="get" action="' + baseUrl + '" id="full-screen-search-form"><div id="full-screen-search-container"><input type="text" name="s" placeholder="Search" id="full-screen-search-input"></div></form></div>';
      $('footer').after(modal);
    },
    actions: function() {
      // ... display the Full Screen search when:
      // 1. The user focuses on a search field, or
      // 2. The user clicks the Search button
      $('#menu-item-search').on('focus, click', function(event) {
        // Prevent the default action
        event.preventDefault();

        // Display the Full Screen Search
        $('#full-screen-search').addClass('open');

        // Focus on the Full Screen Search Input Field
        $('#full-screen-search input').focus();
      });

      // Hide the Full Screen search when the user clicks the close button
      $('#full-screen-search button.close').on('click', function(event) {
        // Prevent the default event
        event.preventDefault();

        // Hide the Full Screen Search
        $('#full-screen-search').removeClass('open');
      });
    }
  };

  // start search
  search.init();

  var rightnav = {
    init: function() {

      // var elm = '.vc_row';
      var elm = '.fd-vc-row';

      // count all rows and place them into an object
      var rows = $(elm);

      if (rows.length > 1) {
        // add rightnav wrapper to main div
        $('.main').prepend('<ul class="vc_rightnav"></ul>');

        // set the height of rightnav based on the number of rows
        $('ul.vc_rightnav').css('height', 20 * (rows.length));

        // add circles to the nav
        rows.each(function(i) {
          var active = (i === 0) ? 'class="active"' : "";
          // create row id and add it to the nav
          $(rows[i]).attr('id','fd-vc-row-'+i);
          $('ul.vc_rightnav').append('<li id="waypoint-' + i + '" ' + active + '><a href="#fd-vc-row-'+i+'" class="scroll_nav"></li>');
        });

        // use waypoints to trigger the active state of the nav circles
        var waypoints = $(elm).waypoint({
          handler: function(direction) {
            $('ul.vc_rightnav li').removeClass('active');
            $('#' + this.key).addClass('active');
          }
        });
      }
    }
  };

  // start circleNav
  rightnav.init();


// Smooth scrolling

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {

    // console.log('click', event, $(this).hasClass('scroll_nav'));
    // On-page links
    if (
      location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname && $(this).hasClass('scroll_nav') ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          }
        });
      }
    }
  })


})(jQuery);
