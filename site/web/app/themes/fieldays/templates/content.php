<?php
$img = has_post_thumbnail()? get_the_post_thumbnail_url($post->ID) : ("/wp-content/themes/fieldays/dist/images/placeholder.svg");
?>

<article <?php post_class();  ?>>
  <a class="thumbnail" href="<?php the_permalink(); ?>">
  	<div class="post-thumbnail" style="background-image:url(<?php echo $img; ?>);">

  	</div>
    </a>
    <div class="post-snip tile-copy">
  		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
      <?php get_template_part('templates/entry-meta'); ?>
  		<p><?php the_excerpt(); ?>
      </p>
    </diV>
</article>
