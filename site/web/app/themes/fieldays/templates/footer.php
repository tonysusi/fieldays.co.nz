<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-lg">
              <?php print_r(wpb_list_footer_menu()); ?>
      </div>
      <div class="col-lg">
          <!-- <div class="d-flex flex-row justify-content-between"> -->
          <div class="footer__logo-newsletter-social-btns">
            <!-- newsletter social btns -->
              <img src="<?= get_template_directory_uri(); ?>/dist/images/NZ-Fieldays-logo.jpg" class="footer__logo--nzFieldaysSociety">

              <form action="https://mysterycreek.us7.list-manage.com/subscribe/post?u=56c197ad937a6078ca1810007&amp;id=f30fb80978" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
              <div class="footer__newsletter input-group">
                <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Sign up to our Newsletter" aria-label="Sign up to our Newsletter">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"><img src="<?= get_template_directory_uri(); ?>/dist/images/radquo.svg" class="footer__newsletter-btn"></button>
                </span>
              </div>
            </form>


              <div class="footer__socialBtns d-flex flex-row ">
              <?php

                // build social links from Social Menu
                $social_items = wp_get_nav_menu_items('Social');

                foreach ($social_items as $social_item) {
                  foreach ($social_item as $key => $value) {
                    if ($key === 'url') {
                      $url = $value;
                    }
                    if ($key === 'title') {
                      $title = strtolower($value);
                    }
                  }
                  echo '<a class="nav-link btn-social btn-'.$title.'" href="'.$url.'"></a>';
                }
              ?>
            </div>
            <!-- newsletter social btns -->
        </div>

        <div class="row footer__socialCopy">
          <div class="col-sm">
            <p>Fieldays is owned and operated by the New Zealand National Fieldays Society. The Society is a not for profit organisation that encourages the growth and development of agriculture in New Zealand.</p>
          </div>
        </div>
        <div class="footer_partnerLogos">
          <div class="row">
            <div class="col-lg">
              <p>Principal Partners</p>
            </div>
          </div>
          <div class="row justify-content-center">
            <img class="partner-logo" src="<?= get_template_directory_uri(); ?>/dist/images/ANZ-brand.svg"</div>
            <img class="partner-logo" src="<?= get_template_directory_uri(); ?>/dist/images/Xero_logo.svg"</div>
            <img class="partner-logo" src="<?= get_template_directory_uri(); ?>/dist/images/vodafone-logo.svg"</div>
        </div>
        </div>
      </div>
    </div>

    <div class="footer__hygenie nav">
      <div class="container">
        <ul>
          <li>Copyright &copy; <?php echo date('Y'); ?> New Zealand National Fieldays Society. All Rights Reserved.</li>
          <?php

            // build legal links from Legal Menu
            $legal_items = wp_get_nav_menu_items('Legal');

            foreach ($legal_items as $legal_item) {
              foreach ($legal_item as $key => $value) {
                if ($key === 'url') {
                  $url = $value;
                }
                if ($key === 'title') {
                  $title = $value;
                }
              }
              echo '<li><a href="'.$url.'">'.$title.'</a></li>';
            }

          ?>

        </ul>
      </div>
    </div>

  </div>
</footer>
