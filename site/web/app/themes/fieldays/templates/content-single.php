<?php
$img = has_post_thumbnail()? get_the_post_thumbnail_url($post->ID) : ("/wp-content/themes/fieldays/dist/images/placeholder.svg");
?>

<?php while (have_posts()) : the_post();?>
  <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="fd-vc-row">
    <div id="post__header--<?php echo $post->ID; ?>>" class="post__header">
      <div class="post__header--inner">
        <!-- Start - SLIDE 1 -->
        <div class="post__header--inner" style="background-image:url(<?php echo $img; ?>);">
          <div class="post__header--caption">
            <h1><?php the_title(); ?></h1>
            </div>
          </div>
          <!-- End - SLIDE 1 -->
        </div>
      </div>
    </div>
  <article <?php post_class(); ?>>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php //comments_template('/templates/comments.php');?>
    <div class="post--nav">
      <a class="btn btn-secondary" href="/latest-news">Back to news</a>
    </div>
  </article>

<?php endwhile; ?>
