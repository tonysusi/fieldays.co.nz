<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php else: ?>

<!-- blog list page -->

<script type="text/javascript">
 jQuery(document).ready(function($) { //wrapper

   function resizeImage() {
     var width = $('body').width();
     var container = $('.main').width();
     var left = (width - container)/2;
    $('.post--head').css({'width': $('body').width()+'px', 'left': '-'+left+'px'});

    }
   window.addEventListener('resize', resizeImage, false);

   resizeImage();

 });

</script>

<div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="fd-vc-row post--head">
    <div id="post__header--398>" class="post__header">
      <div class="post__header--inner">
        <!-- Start - SLIDE 1 -->
        <div class="post__header--inner" style="background-image:url(http://fieldays.co.nz/wp-content/themes/fieldays/dist/images/latest-news-1024x320.jpg);">
          <div class="post__header--caption">
            <h1>Latest news</h1>
            </div>
          </div>
          <!-- End - SLIDE 1 -->
        </div>
      </div>
    </div>


  <div class="grid flex articles column-4 clearfix pad-top-60 pad-bot-60" id="load-posts">

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

</div>

<?php endif; ?>

<?php the_posts_navigation(); ?>
