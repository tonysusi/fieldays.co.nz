<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


/**
 * Footer nav list
 *
 * Grab all pages and format into a list of pages
 * Add the Fieldays svg logo to the begining of the list
 */

function wpb_list_footer_menu() {

 $logo = '<div class="footer__menu-items"><div class="footer__menu-item"><div class="footer__logo"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 146.89 37.58"><defs><style>.cls-1{fill:#000000;}</style></defs><path class="cls-1" d="M141.27,10.73V9.48l2.43-2.63h-2.29V5.51h4.41V6.68L143.37,9.4h2.57v1.33m-6.83,0L137.5,7.88h0v2.85H136V5.51h1.64l1.53,2.79h0V5.51h1.52v5.22Zm8.69-2.61a6.95,6.95,0,1,0-7,7,7,7,0,0,0,7-7" transform="translate(-0.92 -1.17)"/><path class="cls-1" d="M.92,32.81H8.69V23.19H18.77v-6H8.69V14H20.43V7.56H.92m27.51,6.9h-7V32.81h7Zm-7-2h7V7.56h-7Zm15.1,9.09a3.32,3.32,0,0,1,3.61-3.08,3,3,0,0,1,3.08,3.08Zm13.41,3.64C49.93,18.06,46.78,14,39.49,14a9.43,9.43,0,0,0-9.69,9.69c0,6.11,4.42,9.58,10.3,9.58,4.17,0,8-1.84,9.44-5.8H43a3.54,3.54,0,0,1-2.94,1.31c-2.19,0-3.4-1.49-3.57-3.57ZM58.38,7.56h-7V32.81h7Zm21.25,0h-7v8.91h-.07a6,6,0,0,0-5-2.47c-6.16,0-7.82,5.23-7.82,9.55,0,4.59,2.51,9.72,7.71,9.72,3.43,0,4.49-1.31,5.37-2.48h.08v2h6.75ZM72.88,23.62c0,2.19-.57,4.56-3.08,4.56s-3.08-2.37-3.08-4.56.57-4.53,3.08-4.53,3.08,2.37,3.08,4.53M93.1,26.27a2.77,2.77,0,0,1-2.93,2.86A2,2,0,0,1,88,27.44c0-1.24.82-1.63,2.37-2a12,12,0,0,0,2.76-.89Zm6.72-5.09c0-3.54.07-7.18-8.7-7.18-4.35,0-9.23.85-9.44,6h6.51c0-.78.46-1.87,2.61-1.87,1.14,0,2.3.45,2.3,1.69s-.95,1.45-1.91,1.63c-3.57.67-10.36.46-10.36,6.26,0,3.85,2.94,5.55,6.47,5.55,2.27,0,4.42-.5,5.91-2.19h.07a4.72,4.72,0,0,0,.25,1.73h7.14a7.4,7.4,0,0,1-.85-4.1Zm20.69-6.72h-7.25l-3,10.54h-.07l-2.94-10.54H99.79l3.11,8.45c.39,1.06,3.07,7.81,3.07,8.7s-.24,1.16-.67,1.38a5.89,5.89,0,0,1-2.12.17h-1.63v5.59h3.19c2.72,0,5.55,0,7.39-2.26a18.27,18.27,0,0,0,2.3-5.09Zm17.22,5.34c-.18-4.71-4.95-5.8-8.91-5.8-3.68,0-8.67,1.2-8.67,5.83,0,3.15,2.16,4.85,7.5,5.77,3.25.56,3.86.88,3.86,1.94S130.06,29,129.14,29a3,3,0,0,1-1.88-.53,2.27,2.27,0,0,1-.85-1.63h-6.68c.11,4.74,4.85,6.47,9.16,6.47s9.33-1.38,9.33-6.47c0-3-2.05-4.39-4.38-5.13s-5-.92-6.15-1.41c-.39-.18-.82-.43-.82-1,0-1.2,1.13-1.41,2.13-1.41a2.39,2.39,0,0,1,1.55.53,1.76,1.76,0,0,1,.81,1.38Z" transform="translate(-0.92 -1.17)"/><path class="cls-1" d="M140,31.21c.26,0,.52,0,.52-.3s-.2-.3-.41-.3h-.39v.6m0,.95h-.21V30.43h.66c.39,0,.56.17.56.48a.46.46,0,0,1-.43.48l.51.77h-.24l-.49-.77h-.36Zm-.94-.86A1.27,1.27,0,1,0,140.08,30a1.26,1.26,0,0,0-1.27,1.29m2.77,0a1.5,1.5,0,1,1-1.5-1.5,1.47,1.47,0,0,1,1.5,1.5" transform="translate(-0.92 -1.17)"/></svg></div></div>';

  $menuPages = get_pages(array( 'depth' => 1, 'post_parent' => 0, 'sort_column' => 'menu_order'));

  $string = $logo;

  foreach ($menuPages as $key => $page){
    if($page->post_status == 'publish' && $page->post_title != 'Home'){
      $childPages = get_pages( array( 'child_of' => $page->ID));

      // display in footer if value set to true
      $display_in_footer = get_field( "footer_menu_display", $page->ID, true );

      if( $page->post_parent == 0) {
        // $string .='<div class="footer__menu-item"><a href="'.get_page_link($page->ID).'">'.$page->post_title.'</a></div>';
        if(count($childPages) == 0 ){
          $display_in_footer ? $string .='<div class="footer__menu-item--uppercase"><a href="'.get_page_link($page->ID).'">'.$page->post_title.'</a></div>': null ;
        } else {

          $display_in_footer ? $string .='<div class="footer__menu-item--uppercase"><a href="'.get_page_link($page->ID).'">'.$page->post_title.'</a>': $string .='<div class="footer__menu-item--uppercase">' ;
          foreach ($childPages as $key => $cPage){
              if(get_field( "footer_menu_display", $cPage->ID, true )) {$string .='<div class="footer__menu-item"><a href="'.get_page_link($cPage->ID).'">'.$cPage->post_title.'</a></div>'; }
          }
          $string .='</div>';
        }
      }
    }
  }
  $string .='</div>';

  return $string;
}

// add_shortcode('wpb_childpages', 'wpb_list_child_pages');
