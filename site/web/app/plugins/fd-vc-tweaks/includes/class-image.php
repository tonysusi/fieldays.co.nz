<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Image {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_image';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'image-css', $plugin_url . 'css/image.css' );
    }
    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Title', 'fd-vc-image' ),
                'param_name'  => 'title',
                'description' => 'Used in the subnav',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Sub menu', 'fd-vc-image'),
                'param_name' => 'display_submenu',
                'description' => __('Display in sub menu', 'fd-vc-image'),
            ),
            array(
                'type'        => 'attach_image',
                'heading'     => __( 'Header Image', 'fd-vc-image' ),
                'param_name'  => 'image',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'URL', 'fd-vc-image' ),
                'param_name'  => 'url',
                'description' => 'Optional',
                ),
        );

        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Image', 'fd-vc-image' ),
            'description' => __("Single image with Title",'fd-vc-image'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-single-image.svg', dirname( __FILE__ ) ),
        );

        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'image'           => '',
            'title'           => '',
            'url'             => '',
            'display_submenu' => '',
        ) );

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        // Grab the image
        $image = wp_get_attachment_image_src( $data['image'], 'large' );

        $front =  ($data['url']!== "" ?'<a href="'.$data['url'].'">':'');
        $front .= '<div class="vc_image-block vc_image-front" >';
        $front .= '<div class="vc_image-block-inner vc_image-front-inner">';
        $front .= '<h2 class="section-title">'.esc_html( $data['title'] ) .'</h2>';
        $front .= ($data['url']!== "" ?'</div></div></a>':'</div></div>');

        $bgimage ='<div class="vc_image-block_bgimage" style="background-image: url('.$image[0].');"></div>';


        // Start our output
        $output = '';

        // Start our section
        $output .= '<section class="vc_image'. ($data['display_submenu'] ? $submenu : '">');
        $output .= $front;
        $output .= $bgimage;

        $output .= '</section>';
        return $output;


    }
}
