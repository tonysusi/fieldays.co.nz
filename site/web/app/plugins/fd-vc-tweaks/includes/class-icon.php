<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Icon {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_icon';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'icon-css', $plugin_url . 'css/icon.css' );
    }
    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Title', 'fd-vc-icon' ),
                'param_name'  => 'title',
                'description' => 'Used in the subnav and display below icon',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Sub menu', 'fd-vc-icon'),
                'param_name' => 'display_submenu',
                'description' => __('Display in sub menu', 'fd-vc-icon'),
            ),
            array(
                'type'        => 'attach_image',
                'heading'     => __( 'Icon Image', 'fd-vc-icon' ),
                'param_name'  => 'image',
                'description' => '',
            ),
            array(
                'type'        => 'textarea_html',
                'heading'     => __( 'Content', 'fd-vc-icon' ),
                'param_name'  => 'content',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'URL', 'fd-vc-icon' ),
                'param_name'  => 'url',
                'description' => 'Optional',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button text', 'fd-vc-icon' ),
                'param_name'  => 'button_text',
                'description' => 'Optional',
            ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Icon', 'fd-vc-icon' ),
            'description' => __("Icon with text",'fd-vc-icon'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-icon.svg', dirname( __FILE__ ) ),
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'image'           => '',
            'title'           => '',
            'url'             => '',
            'button_text'     => '',
            'display_submenu' => '',
        ) );

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        // Grab the image
        $image = wp_get_attachment_image_src( $data['image'], 'large' );

        // Build our button output
        $button_output = '';
        if ( $data['url'] && $data['button_text'] ) {
            $button_output = '<a href="' . esc_url( $data['url'] ) . '" class="btn btn-primary">' . esc_html( $data['button_text'] ) . '</a>';
        }

        // Start our output
        $output = '';
        // Start our section
        $output .= '<section class="fd-vc-icon'. ($data['display_submenu'] ? $submenu : '">');
        // Output url link
        $output .= $data['url'] ? '<a href="'.$data['url'].'">': '';
        // Output the image if one exists
        $output .= $data['image'] ? '<div class="fd-vc-icon__image" style="background-image:url('.$image[0].')"></div>' : '';
        $output .= $data['url'] ? '</a>':'';
        // Output the title if one exists
        $output .= $data['title'] ? '<h2 class="section-title">' . esc_html( $data['title'] ) . '</h2>' : '';
        // Output the content if it exists
        $output .= $content ? '<p>'.apply_filters( 'the_content', $content ).'</p>' : '';
        // Close our section
        $output .= $button_output;
        $output .= '</section>';
        return $output;
    }
}
