<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Hoverbox {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_hoverbox';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
        // Register js
        add_action( 'init', array( $this, 'load_plugin_js' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'hoverbox-css', $plugin_url . 'css/hoverbox.css' );
    }

    /**
     * Load JS for plugin
     */
    public function load_plugin_js() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_script( 'hoverbox-js', $plugin_url . 'js/hoverbox.js', array('jquery') );
    }

    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Title', 'fd-vc-hoverbox' ),
                'param_name'  => 'title',
                'description' => 'Used in the subnav and over image',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Sub menu', 'fd-vc-image'),
                'param_name' => 'display_submenu',
                'description' => __('Display in sub menu', 'fd-vc-image'),
            ),
            array(
                'type'        => 'attach_image',
                'heading'     => __( 'Header Image', 'fd-vc-hoverbox' ),
                'param_name'  => 'image',
                'description' => '',
            ),
            array(
                'type'        => 'textarea_html',
                'heading'     => __( 'Content', 'fd-vc-hoverbox' ),
                'param_name'  => 'content',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button Text', 'fd-vc-hoverbox' ),
                'param_name'  => 'button_text',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button URL', 'fd-vc-hoverbox' ),
                'param_name'  => 'button_url',
                'description' => '',
            ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Hoverbox', 'fd-vc-hoverbox' ),
            'description' => __("Hover over image to reveal more text",'fd-vc-hoverbox'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-hover-box.svg', dirname( __FILE__ ) ),
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'image'           => '',
            'title'           => '',
            'button_url'      => '',
            'button_text'     => '',
            'display_submenu' => '',
        ) );

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        // Grab the image
        $image = wp_get_attachment_image_src( $data['image'], 'large' );


        $bgimage ='<div class="vc_hoverbox-block_bgimage" style="background-image: url('.$image[0].');"></div>';

        // Build our button output
        $button_output = '';
        if ( $data['button_url'] && $data['button_text'] ) {
            $button_output = '<a href="' . esc_url( $data['button_url'] ) . '" class="btn btn-secondary">' . esc_html( $data['button_text'] ) . '</a>';
        }


        // Start our output
        $output = '';
        // Start our section

        $output .= '<section class="vc_hoverbox'. ($data['display_submenu'] ? $submenu : '">');
        // $output .= $front;
        $output .= '<div class="vc_hoverbox-block vc_hoverbox-front">';
        $output .= '<div class="vc_hoverbox-block-inner vc_hoverbox-front-inner">';
        $output .= '<h2 class="section-title">'.esc_html( $data['title'] ) .'</h2>';
        $output .= '</div></div>';

        // $output .= $back;
        $output .= '<div class="vc_hoverbox-block vc_hoverbox-back"><div class="vc_hoverbox-block-inner vc_hoverbox-back-inner">';
        // $output .= '<a href="'.esc_url( $data['button_url'] ) .'">';
        $output .= '<h2 class="title">'.esc_html( $data['title'] ).'</h2>';
        $output .= $content ? '<p>'.apply_filters( 'the_content', $content ).'</p>' : '';
        $output .= $button_output;
        // $output .= '</a>';
        $output .= '</div></div>';

        // $output .= $bgimage;
        // $output .= '<a href="'.esc_url( $data['button_url'] ) .'">';
        $output .='<div class="vc_hoverbox-block_bgimage" style="background-image: url('.$image[0].');"></div>';
        // $output .= '</a>';

        // $output .= '<a href="'.esc_url( $data['button_url'] ) .'">';

        // Close our section
        $output .= '</section>';

        return $output;

    }
}
