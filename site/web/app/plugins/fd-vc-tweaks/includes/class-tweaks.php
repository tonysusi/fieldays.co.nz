<?php
/**
 * FD Visual Composer Tweaks
 * @version 0.1.0
 * @package FD AC VC
 */
class FDVC_Tweaks {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Initiate our hooks
     *
     * @since  0.1.0
     * @return void
     */
    public function hooks() {
        // Whitelist the elements we want to keep
        add_filter( 'plugins_loaded', array( $this, 'whitelist' ), 999 );

        // Filter to replace default css class names for vc_row shortcode and vc_column
        add_filter( 'vc_shortcodes_css_class', array( $this, 'custom_container_classes' ), 10, 2 );
    }
    /**
     * Removes "Grid Elements" from the primary admin menu
     **/
    public function remove_grid_elements_menu_item() {
        remove_menu_page( 'edit.php?post_type=vc_grid_item' );
    }
    /**
     * Stops Visual Composer from prompting for an update
     *
     * @param object
     * @return object
     **/
    public function no_updates_for_vc( $value ) {
        if ( empty( $value ) || empty( $value->response['js_composer/js_composer.php'] ) ) {
            return $value;
        }
        unset( $value->response['js_composer/js_composer.php'] );
        return $value;
    }
     /**
     * Remove an anonymous object filter.
     *
     * "It goes down to the dark heart of the plugin API and best programming practices."
     * https://wordpress.stackexchange.com/a/57088
     *
     * @param  string $tag    Hook name.
     * @param  string $class  Class name
     * @param  string $method Method name
     * @return void
     */
    public function remove_anonymous_object_filter( $tag, $class, $method ) {
        $filters = $GLOBALS['wp_filter'][ $tag ];
        if ( empty( $filters ) ) {
            return;
        }
        foreach ( $filters as $priority => $filter ) {
            foreach ( $filter as $identifier => $function ) {
                if ( is_array( $function )
                    and is_a( $function['function'][0], $class )
                    and $method === $function['function'][1]
                ) {
                    remove_filter(
                        $tag,
                        array( $function['function'][0], $method ),
                        $priority
                    );
                }
            }
        }
    }
    /**
     * Hook in our anonymous_object_filter and kill the VC exceprt filtering
     * @param  [type] $content [description]
     * @return [type]          [description]
     */
    public function kill_excerpt_filter( $content ) {
        $this->remove_anonymous_object_filter(
            'the_excerpt',
            'Vc_Base',
            'excerptFilter'
        );
        return $content;
    }
    /**
     * Hook in our anonymous_object_filter and kill the VC admin_bar_menu
     * @param  [type] $content [description]
     * @return [type]          [description]
     */
    public function kill_admin_bar_menu( $content ) {
        $this->remove_anonymous_object_filter(
            'admin_bar_menu',
            'Vc_Frontend_Editor',
            'adminBarEditLink'
        );
        return $content;
    }
    /**
     * Enqueue our admin style
     */
    public function admin_styles() {
        wp_enqueue_style( 'fd-vc-admin', plugins_url( 'fd-vc-tweaks/assets/admin.css' ), '', '1.0.0' );
    }
    /**
     * Gets a list of the shortcodes supplied with Visual Composer
     *
     * @return array
     **/
    private function get_builtin_shortcodes() {
        $dir = scandir( WP_PLUGIN_DIR . '/js_composer/include/templates/shortcodes' );
        $shortcodes = array();
        foreach ( $dir as $file ) {
            $shortcodes[] = trim( str_replace( '.php', '', $file ) );
        }
        return $shortcodes;
    }
    /**
     * Whitelist a select set of VC elements
     * @param  array $elements Array of all VC elements
     * @return array           Array of whitelisted VC elements
     */
    public function whitelist( $elements ) {
        $whitelist = array(
            'vc_row',             // Basic VC Row
            'vc_column',          // Basic VC Column
            'vc_single_image',    // Single Image
            'vc_text_separator',  // Heading with horizontal rules on either side
            'vc_column_text',     // Basic column text
            'vc_separator',       // Horizontal Rule with custom styling options
            'vc_tta_section',     // Section used to build Tabs, Pageable Content (carousel), Tour, and Accordion elements
            'vc_raw_html',        // Html block of code
            'vc_row_inner',       // * Needs to added or renders nothing
            'vc_column_inner',
            // 'vc_wp_search',
            // 'vc_carousel',
            // 'vc_images_carousel',
            // 'vc_masonry_grid',
            // 'vc_masonry_media_grid',
            // 'vc_posts_grid',
            // 'vc_posts_slider',
            // 'vc_gitem',
            // 'vc_gitem_animated_block',
            // 'vc_gitem',
            // 'vc_gitem_animated_block',
            // 'vc_gitem_block',
            // 'vc_gitem_col',
            // 'vc_gitem_image',
            // 'vc_gitem_post_author',
            // 'vc_gitem_post_categories',
            // 'vc_gitem_post_data',
            // 'vc_gitem_post_meta',
            // 'vc_gitem_row',
            // 'vc_gitem_zone',
            // 'vc_gitem_zone_c',
        );

        // Get list of builtin shortcodes
        foreach ( $this->get_builtin_shortcodes() as $s ) {
            if ( in_array( $s, $whitelist ) ) {
                continue;
                // Do not remove whitelisted shortcodes
            }
            vc_remove_element( $s );
        }

        // Remove elements the above didn't catch
        $to_remove = array(
            'vc_media_grid',
            'vc_raw_js',
            'vc_tta_tour',
            'vc_tta_pageable',
            'vc_tta_accordion',
            'vc_tta_tabs',
            'vc_text_separator',
            'vc_separator',
            'vc_tour',
            'vc_accordion',
            'vc_accordion_tab',
            'vc_basic_grid',
            'vc_basic_grid_filter',
            // 'vc_carousel',
            'vc_images_carousel',
            'vc_masonry_grid',
            'vc_masonry_media_grid',
            // 'vc_btn',
            //   'vc_button',
            //   'vc_button2',
            //   'vc_carousel',
            //   'vc_column_inner',
            //   'vc_cta',
            //   'vc_cta_button',
            //   'vc_cta_button2',
            //   'vc_custom_field',
            //   'vc_custom_heading',
            //   'vc_empty_space',
            //   'vc_facebook',
            //   'vc_flickr',
            //   'vc_gallery',
            //   'vc_gitem',
            //   'vc_gitem_animated_block',
            //   'vc_gitem_block',
            //   'vc_gitem_col',
            //   'vc_gitem_image',
            //   'vc_gitem_post_author',
            //   'vc_gitem_post_categories',
            //   'vc_gitem_post_data',
            //   'vc_gitem_post_meta',
            //   'vc_gitem_row',
            //   'vc_gitem_zone',
            //   'vc_gitem_zone_c',
            //   'vc_gmaps',
            //   'vc_googleplus',
            //   // 'vc_hoverbox',
            //   'vc_icon',
            //   'vc_images_carousel',
            //   'vc_item',
            //   'vc_items',
            //   'vc_line_chart',
            //   'vc_message',
            //   'vc_pie',
            //   'vc_pinterest',
            //   'vc_posts_grid',
            //   'vc_posts_slider',
            //   'vc_progress_bar',
            //   // 'vc_raw_html',
            //   'vc_round_chart',
            //   // 'vc_row_inner',
            //   'vc_section',
            //   'vc_tab',
            //   'vc_tabs',
            //   'vc_toggle',
            //   'vc_tta_global',
            //   'vc_tta_pageable_section',
            //   'vc_tweetmeme',
            //   // 'vc_video',
            //   'vc_widget_sidebar',
            //   'vc_wp_archives',
            //   'vc_wp_calendar',
            //   'vc_wp_categories',
            //   'vc_wp_custommenu',
            //   'vc_wp_links',
            //   'vc_wp_meta',
            //   'vc_wp_pages',
            //   'vc_wp_posts',
            //   'vc_wp_recentcomments',
            //   'vc_wp_rss',
            //   'vc_wp_search',
            //   'vc_wp_tagcloud',
            //   'vc_wp_text',
            //   'vc_zigzag'
        );
        foreach ( $to_remove as $r ) {
            vc_remove_element( $r );
        }
    }
    /**
     * Remove the standard VC classes used for rows and columns
     * so we can use our own classes and baseline styles
     * https://wpbakery.atlassian.net/wiki/display/VC/Change+CSS+Class+Names+in+Output
     *
     * @author Corey M Collins <corey@webdevstudios.com>
     */
    function custom_container_classes( $class_string, $tag ) {
        // First, remove all standard VC parent container classes
        $class_string = str_replace( array( 'vc_row', 'wpb_row', 'vc_row-fluid', 'wpb_column', 'vc_column_container' ), '', $class_string );
        // For the row
        // Add our custom class in a filter so it can be easily changed
        if ( 'vc_row' == $tag || 'vc_row_inner' == $tag ) {
            $class_string = apply_filters( 'fd_vc_custom_row_class', 'fd-vc-row' );
        }
        // For the column
        // This will replace "vc_col-sm-%" with filterable custom class
        if ( 'vc_column' == $tag || 'vc_column_inner' == $tag ) {
            $class_string = preg_replace( '/vc_col-sm-(\d{1,2})/', apply_filters( 'fd_vc_custom_row_class', 'fd-vc-column fd-vc-column-$1' ), $class_string );
        }
        // Return our classes
        return $class_string;
    }
}
