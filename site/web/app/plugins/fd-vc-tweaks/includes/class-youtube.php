<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Youtube {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_ad';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
        // Register js
        add_action( 'init', array( $this, 'load_plugin_js' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'youtube-css', $plugin_url . 'css/youtube.css' );
    }

    /**
     * Load JS for plugin
     */
    public function load_plugin_js() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_script( 'youtube-js', $plugin_url . 'js/youtube.js', array('jquery') );
    }

    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Title', 'fd-vc-youtube' ),
                'param_name'  => 'title',
                'description' => 'Used in the subnav and the title',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Sub menu', 'fd-vc-image'),
                'param_name' => 'display_submenu',
                'description' => __('Display in sub menu', 'fd-vc-image'),
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Youtube URL', 'fd-vc-youtube' ),
                'param_name'  => 'video_url',
                'description' => 'Example: https://www.youtube.com/watch?v=e0-MOj7nJHY',
            ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Video Player', 'fd-vc-youtube' ),
            'description' => __("Embed YouTube player",'fd-vc-youtube'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-video.svg', dirname( __FILE__ ) ),
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'title'           => '',
            'video_url'       => '',
            'display_submenu' => '',
        ) );

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        /* ----------------
        // Grab video id from youtube url
        */
        $video_remove = array("");
        $video_id = str_replace('https://www.youtube.com/watch?v=','',$data['video_url']);


        // Start our output
        $output = '<section class="vc_youtube '.($data['display_submenu'] ? $submenu : '">');

        $output .= '<div class="video embed-responsive embed-responsive-16by9">';
        // $output .= '<img src="https://img.youtube.com/vi/'.$video_id.'/hqdefault.jpg">';
        // $output .= ' <iframe  width="940" height="529" src="http://www.youtube.com/embed/'.$video_id.'?autoplay=0&rel=0&showinfo=0&controls=1" frameborder="0" allowfullscreen></iframe>';
        $output .= ' <iframe  width="940" height="529" src="http://www.youtube.com/embed/'.$video_id.'?feature=oembed&autoplay=0&rel=0&showinfo=0&controls=1" frameborder="0" allowfullscreen=""></iframe>';
        $output .= '</div>';

        // Close our section
        $output .= '</section>';

        return $output;
    }
}
