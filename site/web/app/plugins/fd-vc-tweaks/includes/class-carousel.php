<?php
/**
 * Fieldays Header Carousel
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Carousel
{
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct($plugin)
    {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_carousel';
    public function hooks()
    {
        // Register (map) the new VC module
        add_action('vc_before_init', array( $this, 'vc_map' ));
        // Register the block as a shortcode - Required to display!
        add_action('init', array( $this, 'register_shortcode' ));
        // Register css
        add_action('init', array( $this, 'load_plugin_css' ));
        // Register js
        add_action( 'init', array( $this, 'load_plugin_js' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode()
    {
        add_shortcode($this->element_name, array( $this, 'render_block' ));
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css()
    {
        $plugin_url = plugin_dir_url(__FILE__);
        wp_enqueue_style('carousel-css', $plugin_url . 'css/carousel.css');
    }

    /**
     * Load JS for plugin
     */
    public function load_plugin_js() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_script( 'carousel-js', $plugin_url . 'js/carousel.js', array('jquery') );
    }

    /**
     * Setup block defaults.
     */
    public function vc_map()
    {
        $fields = array(
          array(
            'type' => 'checkbox',
            'heading' => __('Include sub menu', 'js_composer'),
            'param_name' => 'use_sub_menu',
            'description' => __('Add a sub menu below the carousel', 'js_composer'),
            "group" => "Slide 1"
          ),
          //SLIDE 1
            array(
                'type'        => 'attach_image',
                'heading'     => __('Header Image', 'fd-vc-carousel'),
                'param_name'  => 'image_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Top text', 'fd-vc-carousel'),
                'param_name'  => 'top_text_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Title', 'fd-vc-carousel'),
                'param_name'  => 'title_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Bottom text', 'fd-vc-carousel'),
                'param_name'  => 'bottom_text_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Green Button Text', 'fd-vc-carousel'),
                'param_name'  => 'green_button_text_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Green Button URL', 'fd-vc-carousel'),
                'param_name'  => 'green_button_url_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Green Button Link target','fd-vc-carousel'),
               'param_name' => 'green_button_link_target_s1',
               'value' => array(
                 __('Same window','fd-vc-carousel')  => '_self',
                 __('New window','fd-vc-carousel')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
               'group' => 'Slide 1'
             ),
             array(
                 'type'        => 'textfield',
                 'heading'     => __('Grey Button Text', 'fd-vc-carousel'),
                 'param_name'  => 'grey_button_text_s1',
                 'description' => '',
                 "group" => "Slide 1"
             ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Grey Button URL', 'fd-vc-carousel'),
                'param_name'  => 'grey_button_url_s1',
                'description' => '',
                "group" => "Slide 1"
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Grey Button Link target','fd-vc-carousel'),
               'param_name' => 'grey_button_link_target_s1',
               'value' => array(
                 __('Same window','fd-vc-carousel')  => '_self',
                 __('New window','fd-vc-carousel')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
               'group' => 'Slide 1'
             ),
            // SLIDE 2
             array(
               "type" => "dropdown",
               "heading" => __("Show in carousel", "fd-vc-carousel"),
               "param_name" => "s2_visable",
               "value" => array(
                 __("No", "fd-vc-carousel") => 'false',
                 __("Yes", "fd-vc-carousel") => 'true',
               ),
               "group" => "Slide 2"
             ),
            array(
                'type'        => 'attach_image',
                'heading'     => __('Header Image', 'fd-vc-carousel'),
                'param_name'  => 'image_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Top text', 'fd-vc-carousel'),
                'param_name'  => 'top_text_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Title', 'fd-vc-carousel'),
                'param_name'  => 'title_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Bottom text', 'fd-vc-carousel'),
                'param_name'  => 'bottom_text_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Green Button Text', 'fd-vc-carousel'),
                'param_name'  => 'green_button_text_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Green Button URL', 'fd-vc-carousel'),
                'param_name'  => 'green_button_url_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Green Button Link target','fd-vc-carousel'),
               'param_name' => 'green_button_link_target_s2',
               'value' => array(
                 __('Same window','fd-vc-carousel')  => '_self',
                 __('New window','fd-vc-carousel')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
               'group' => 'Slide 2'
             ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Grey Button Text', 'fd-vc-carousel'),
                'param_name'  => 'grey_button_text_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Grey Button URL', 'fd-vc-carousel'),
                'param_name'  => 'grey_button_url_s2',
                'description' => '',
                "group" => "Slide 2"
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Grey Button Link target','fd-vc-carousel'),
               'param_name' => 'grey_button_link_target_s2',
               'value' => array(
                 __('Same window','fd-vc-carousel')  => '_self',
                 __('New window','fd-vc-carousel')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
               'group' => 'Slide 2'
             ),
            // SLIDE 3
            array(
              "type" => "dropdown",
              "heading" => __("Show in carousel", "fd-vc-carousel"),
              "param_name" => "s3_visable",
              "value" => array(
                __("No", "fd-vc-carousel") => 'false',
                __("Yes", "fd-vc-carousel") => 'true',
              ),
              "group" => "Slide 3"
            ),
            array(
                'type'        => 'attach_image',
                'heading'     => __('Header Image', 'fd-vc-carousel'),
                'param_name'  => 'image_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Top text', 'fd-vc-carousel'),
                'param_name'  => 'top_text_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Title', 'fd-vc-carousel'),
                'param_name'  => 'title_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Bottom text', 'fd-vc-carousel'),
                'param_name'  => 'bottom_text_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Green Button Text', 'fd-vc-carousel'),
                'param_name'  => 'green_button_text_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Green Button URL', 'fd-vc-carousel'),
                'param_name'  => 'green_button_url_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Green Button Link target','fd-vc-carousel'),
               'param_name' => 'green_button_link_target_s3',
               'value' => array(
                 __('Same window','fd-vc-carousel')  => '_self',
                 __('New window','fd-vc-carousel')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
               'group' => 'Slide 3'
             ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Grey Button Text', 'fd-vc-carousel'),
                'param_name'  => 'grey_button_text_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __('Grey Button URL', 'fd-vc-carousel'),
                'param_name'  => 'grey_button_url_s3',
                'description' => '',
                "group" => "Slide 3"
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Grey Button Link target','fd-vc-carousel'),
               'param_name' => 'grey_button_link_target_s3',
               'value' => array(
                 __('Same window','fd-vc-carousel')  => '_self',
                 __('New window','fd-vc-carousel')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
               'group' => 'Slide 3'
             ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __('Header Carousel', 'fd-vc-carousel'),
            'description' => __("Carousel at the top of the page", 'fd-vc-carousel'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url('assets/images/vc-icon-header-carousel.png', dirname(__FILE__)),
        );
        // Register block with Visual Composer.
        vc_map($args);
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block($atts, $content = null)
    {
        $data = wp_parse_args($atts, array(
            'use_sub_menu'         => '',
            'image_s1'             => '',
            'title_s1'             => '',
            'top_text_s1'          => '',
            'bottom_text_s1'       => '',
            'green_button_url_s1'  => '',
            'green_button_text_s1' => '',
            'green_button_link_target_s1' => '',
            'grey_button_url_s1'   => '',
            'grey_button_text_s1'  => '',
            'grey_button_link_target_s1'  => '',
            'image_s2'             => '',
            'title_s2'             => '',
            'top_text_s2'          => '',
            'bottom_text_s2'       => '',
            'green_button_url_s2'  => '',
            'green_button_text_s2' => '',
            'green_button_link_target_s2' => '',
            'grey_button_url_s2'   => '',
            'grey_button_text_s2'  => '',
            'grey_button_link_target_s2'  => '',
            'image_s3'             => '',
            'title_s3'             => '',
            'top_text_s3'          => '',
            'bottom_text_s3'       => '',
            'green_button_url_s3'  => '',
            'green_button_text_s3' => '',
            'green_button_link_target_s3' => '',
            'grey_button_url_s3'   => '',
            'grey_button_text_s3'  => '',
            'grey_button_link_target_s3'  => '',
            's2_visable'           => '',
            's3_visable'           => '',
        ));
        // Grab the images
        $image_src_s1 = wp_get_attachment_image_src($data['image_s1'], 'large');
        $image_src_s2 = wp_get_attachment_image_src($data['image_s2'], 'large');
        $image_src_s3 = wp_get_attachment_image_src($data['image_s3'], 'large');

        // Build our button output
        $button_output_s1 = '<div class="carousel-buttons">';
        if ($data['green_button_url_s1'] && $data['green_button_text_s1']) {
            $button_output_s1 .= '<a href="' . esc_url($data['green_button_url_s1']) . '" class="btn btn-primary" target="'.$data['green_button_link_target_s1'].'">' . esc_html($data['green_button_text_s1']) . '</a>';
        }
        if ($data['grey_button_url_s1'] && $data['grey_button_text_s1']) {
            $button_output_s1 .= '<a href="' . esc_url($data['grey_button_url_s1']) . '" class="btn btn-secondary" target="'.$data['grey_button_link_target_s1'].'">' . esc_html($data['grey_button_text_s1']) . '</a>';
        }
        $button_output_s1 .='</div>';


        // Build our button output
        $button_output_s2 = '<div class="carousel-buttons">';
        if ($data['green_button_url_s2'] && $data['green_button_text_s2']) {
            $button_output_s2 .= '<a href="' . esc_url($data['green_button_url_s2']) . '" class="btn btn-primary" target="'.$data['green_button_link_target_s2'].'">' . esc_html($data['green_button_text_s2']) . '</a>';
        }
        if ($data['grey_button_url_s2'] && $data['grey_button_text_s2']) {
            $button_output_s2 .= '<a href="' . esc_url($data['grey_button_url_s2']) . '" class="btn btn-secondary" target="'.$data['grey_button_link_target_s2'].'">' . esc_html($data['grey_button_text_s2']) . '</a>';
        }
        $button_output_s2 .='</div>';

        // Build our button output
        $button_output_s3 = '<div class="carousel-buttons">';
        if ($data['green_button_url_s3'] && $data['green_button_text_s3']) {
            $button_output_s3 .= '<a href="' . esc_url($data['green_button_url_s3']) . '" class="btn btn-primary" target="'.$data['green_button_link_target_s3'].'">' . esc_html($data['green_button_text_s3']) . '</a>';
        }
        if ($data['grey_button_url_s3'] && $data['grey_button_text_s3']) {
            $button_output_s3 .= '<a href="' . esc_url($data['grey_button_url_s3']) . '" class="btn btn-secondary" target="'.$data['grey_button_link_target_s3'].'">' . esc_html($data['grey_button_text_s3']) . '</a>';
        }
        $button_output_s3 .='</div>';

        //Find out slide numbers
        if ($data['s2_visable'] == 'true' || $data['s3_visable'] == 'true') {
            if ($data['s2_visable'] == 'true' && $data['s3_visable'] == 'true') {
                $slide_numbers = 3;
            } else {
                $slide_numbers = 2;
            }
        } else {
            $slide_numbers = 1;
        }

        //Build indicators
        switch ($slide_numbers) {
            case 1:
                $indicators = '';
                break;
            case 2:
                $indicators = '<ol class="carousel-indicators"><li data-target="#header__carousel--fieldays" data-slide-to="0" ></li><li data-target="#header__carousel--fieldays" data-slide-to="1"></li></ol>';
                break;
          case 3:
            $indicators = '<ol class="carousel-indicators"><li data-target="#header__carousel--fieldays" data-slide-to="0" ></li><li data-target="#header__carousel--fieldays" data-slide-to="1"></li><li data-target="#header__carousel--fieldays" data-slide-to="2"></li></ol>';
            break;
            default:
                // default code block
                break;
        }


        $slide_array = array();
        for($i=0;$i<3;$i++){
          $j = $i+1;
          switch ($i) {
            case 0:
              $image_url = $image_src_s1[0];
              $image_height = $image_src_s1[2];
              $button = $button_output_s1;
              $html = '<div class="carousel-item active" style="background-image:url('.$image_url.'); height:'.$image_height.'px;">';
              break;
            case 1:
              $image_url = $image_src_s2[0];
              $image_height = $image_src_s2[2];
              $button = $button_output_s2;
              $html = '<div class="carousel-item" style="background-image:url('.$image_url.'); height:'.$image_height.'px;">';
              break;
            case 2:
              $image_url = $image_src_s3[0];
              $image_height = $image_src_s3[2];
              $button = $button_output_s3;
              $html = '<div class="carousel-item" style="background-image:url('.$image_url.'); height:'.$image_height.'px;">';
              break;
          }
          $html .= '<div class="carousel-caption">';
          $html .= '<p>'.$data['top_text_s'.$j].'</p>';
          $html .= '<h1>'.$data['title_s'.$j].'</h1>';
          $html .= '<p>'.$data['bottom_text_s'.$j].'</p>';
          $html .=  $button;
          $html .= '</div>';
          $html .= '</div>';
          $slide_array[] = $html;
        }

        // var_dump($slide_array);


        // Start our output
        $output = '';

        $output .= '<div id="header__carousel--fieldays" class="carousel slide" data-ride="carousel">';
        $output .= $indicators;
        $output .= '<div class="carousel-inner">';
        // Start - SLIDE 1
        $output .= '<!-- Start - SLIDE 1 -->';
        $output .= $slide_array[0];
        $output .= '<!-- End - SLIDE 1 -->';
        // End - SLIDE 1

        if ($slide_numbers == 2) {
            // Start - SLIDE 2
            if ($data['s2_visable'] == 'true') {
                $output .= '<!-- Start - SLIDE 2 -->';
                $output .= $slide_array[1];
                $output .= '<!-- End - SLIDE 2 -->';
            } else {
                $output .= '<!-- Start - SLIDE 2 -->';
                $output .= $slide_array[2];
                $output .= '<!-- End - SLIDE 2 -->';
            }
            // End - SLIDE 2
        }

        if ($slide_numbers == 3) {
            $output .= '<!-- Start - SLIDE 2 -->';
            $output .= $slide_array[1];
            $output .= '<!-- End - SLIDE 2 -->';
            // Start - SLIDE 3
            $output .= '<!-- Start - SLIDE 3 -->';
            $output .= $slide_array[2];
            $output .= '<!-- End - SLIDE 3 -->';
        }
        // End - SLIDE 3
        $output .= '</div>';

        // if more than one slide, render the control arrows
        if ($slide_numbers > 1) {
            $output .= '<a class="carousel-control-prev" href="#header__carousel--fieldays" role="button" data-slide="prev">';
            $output .= '<span class="carousel-control-prev-icon" aria-hidden="true"></span>';
            $output .= '<span class="sr-only">Previous</span>';
            $output .= '</a>';
            $output .= '<a class="carousel-control-next" href="#header__carousel--fieldays" role="button" data-slide="next">';
            $output .= '<span class="carousel-control-next-icon" aria-hidden="true"></span>';
            $output .= '<span class="sr-only">Next</span>';
            $output .= '</a>';
        }
        $output .= '</div>';

        if ($data['use_sub_menu'] == 'true') {
            $output .= '<div class="header__submenu"><ul class="nav"></ul></div>';
        }

        return $output;
    }
}
