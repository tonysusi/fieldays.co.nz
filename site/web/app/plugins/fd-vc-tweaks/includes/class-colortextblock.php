<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_ColorTextBlock {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_colorTextBlock';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
        // Register js
        add_action( 'init', array( $this, 'load_plugin_js' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'colortextblock-css', $plugin_url . 'css/colortextblock.css' );
    }

    /**
     * Load JS for plugin
     */
    public function load_plugin_js() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_script( 'colortextblock-js', $plugin_url . 'js/colortextblock.js', array('jquery') );
    }

    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
              "type" => "dropdown",
              "heading" => __("Background color","fd-vc-color-text-block"),
              "param_name" => "bg_color",
              "value" => array(
                __("Green","fd-vc-color-text-block") => 'green',
                __("Grey","fd-vc-color-text-block") => 'grey',
                __("White","fd-vc-color-text-block") => 'white',
              ),
              'description' => 'Green, grey or white background',
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Alignment","fd-vc-button"),
                "param_name" => "content_alignment",
                "value" => array(
                  __("Left","fd-vc-button") => 'content--left',
                  __("Center","fd-vc-button") => 'content--center',
                  __("Right","fd-vc-button") => 'content--right',
                ),
                'description' => 'Align content left, center or right',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Title', 'fd-vc-color-text-block' ),
                'param_name'  => 'title',
                'description' => 'Used in the subnav and the title',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Sub menu', 'fd-vc-image'),
                'param_name' => 'display_submenu',
                'description' => __('Display in sub menu', 'fd-vc-image'),
            ),
            array(
                'type'        => 'textarea_html',
                'heading'     => __( 'Content', 'fd-vc-color-text-block' ),
                'param_name'  => 'content',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button Text', 'fd-vc-color-text-block' ),
                'param_name'  => 'button_text',
                'description' => 'Optional',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button URL', 'fd-vc-color-text-block' ),
                'param_name'  => 'button_url',
                'description' => 'Ex: "http://www.fieldays.co.nz" or "/home"',
            ),
            array(
               'type' => 'dropdown',
               'heading' => __('Link target','fd-vc-color-text-block'),
               'param_name' => 'link_target',
               'value' => array(
                 __('Same window','fd-vc-color-text-block')  => '_self',
                 __('New window','fd-vc-color-text-block')  => '_blank',
               ),
               'std'  => '_self',
               'description' => '',
             ),
            array(
                'type' => 'dropdown',
                'heading' => __('Button color','js_composer'),
                'param_name' => 'button_color',
                'value' => array(
                  __('Green','js_composer') => 'primary',
                  __('Grey','js_composer') => 'secondary',
                ),
                'description' => 'Green or grey button',
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Button alignment','fd-vc-color-text-block'),
                'param_name' => 'button_alignment',
                'value' => array(
                  __('Left','fd-vc-color-text-block') => 'button--left',
                  __('Center','fd-vc-color-text-block') => 'button--center',
                  __('Right','fd-vc-color-text-block') => 'button--right',
                ),
                'description' => 'Align button left, center or right',
            ),
            array(
              'type' => 'checkbox',
              'heading' => __('Full height', 'js_composer'),
              'param_name' => 'full_height',
              'description' => 'Expand to the full height of the text',
            ),
            array(
              'type' => 'checkbox',
              'heading' => __('Remove bottom padding', 'js_composer'),
              'param_name' => 'no_bottom_padding',
              'description' => 'useful when a button follows',
            ),
            array(
              'type' => 'checkbox',
              'heading' => __('Read more', 'js_composer'),
              'param_name' => 'read_more',
              'description' => 'Add a read more toggle button (will overwrite full height setting)',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Inline Style', 'fd-vc-color-text-block' ),
                'param_name'  => 'inline_style',
                'description' => 'Inline CSS style (Optional)',
            ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Color Text Block', 'fd-vc-color-text-block' ),
            'description' => __("Color backgound with button",'fd-vc-color-text-block'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-cta.svg', dirname( __FILE__ ) ),
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'bg_color'          => '',
            'title'             => '',
            'button_url'        => '',
            'button_text'       => '',
            'button_color'      => '',
            'button_alignment'  => '',
            'inline_style'      => '',
            'full_height'       => '',
            'display_submenu'   => '',
            'read_more'         => '',
            "content_alignment" => '',
            "no_bottom_padding" => '',
            'link_target'       => '',
        ) );

        switch($data['bg_color']){
          case 'green':
            $bg_color = 'green-bg';
            break;
          case 'grey':
            $bg_color = 'grey-bg';
            break;
          case 'white':
            $bg_color = 'white-bg';
            break;
          default:
            $bg_color = '';
            break;
        }

        $fullHeight = ($data['full_height'] ? ($data['read_more'] ? '' : 'full-height' ) : '');

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        $readMore = '<div class="vc_color-text-block__btn"><a class="btn btn-secondary">Read more</a></div>';

        $buttonColor = ($data['button_color'] === '' ? 'btn-primary' : 'btn-secondary' );

        $linkTarget = ($data['link_target'] === '' ? '_self' : '_blank' );

        $noBottomPadding = ($data['no_bottom_padding'] ? 'no-bottom-padding' : '');

        // Build our button output
        $button_output = '';
        if ( $data['button_url'] && $data['button_text'] ) {
            $button_output = '<div class="vc_button '.$data['button_alignment'].'"><a href="' . esc_url( $data['button_url'] ).'" class="btn '.$buttonColor.'"  target= "'.$linkTarget.'">' . esc_html( $data['button_text'] ) . '</a></div>';
        }

        // Start our output
        $output = '';
        // Start our section
        $output .= '<section style="'.$data['inline_style'].'" class="vc_color-text-block '.$data['content_alignment'].' '.$bg_color.' '.$fullHeight.' '.$noBottomPadding.' '.($data['display_submenu'] ? $submenu : '">');
        $output .= '<div class="vc_color-text-block__content">';
        // Output the title if one exists
        $output .= $data['title'] ? '<h2 class="title">' . esc_html( $data['title'] ) . '</h2>' : '';
        // Output the content if it exists
        $output .= $content ? '<p>'.apply_filters( 'the_content', $content ).'</p>' : '';

        $output .= $button_output;

        $output .= ($data['read_more'] ? '</div>'.$readMore : '</div>' );
        // Close our section
        $output .= '</section>';

        return $output;
    }
}
