<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Iframe {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_iframe';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        // add_action( 'init', array( $this, 'load_plugin_css' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    // public function load_plugin_css() {
    //     $plugin_url = plugin_dir_url( __FILE__ );
    //     wp_enqueue_style( 'iframe-css', $plugin_url . 'css/iframe.css' );
    // }

    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Title', 'fd-vc-iframe' ),
                'param_name'  => 'title',
                'description' => 'Used in the subnav and display below iframe',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Sub menu', 'fd-vc-image'),
                'param_name' => 'display_submenu',
                'description' => __('Display in sub menu', 'fd-vc-image'),
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'URL', 'fd-vc-iframe' ),
                'param_name'  => 'url',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Height', 'fd-vc-iframe' ),
                'param_name'  => 'height',
                'description' => 'Height of the iFrame in px',
            ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Iframe', 'fd-vc-iframe' ),
            'description' => __("Iframe in a website",'fd-vc-iframe'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-iframe.svg', dirname( __FILE__ ) ),
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'title'       => '',
            'url'        => '',
            'height'     => '',
            'display_submenu'   => '',
        ) );

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        // Start our output
        $output = '';
        // Start our section
        $output .= '<section class="fd-vc-iframe'. ($data['display_submenu'] ? $submenu : '">');

        $output .= '<iframe src="'.$data['url'].'" style="border:0px #ffffff none;" name="myiFrame" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="'.$data['height'].'px" width="100%" allowfullscreen></iframe>';

        // Close our section
        $output .= '</section>';
        return $output;
    }
}
