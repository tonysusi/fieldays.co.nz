<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Countdown {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'vc_countdown';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
        // Register js
        add_action( 'init', array( $this, 'load_plugin_js' ) );
    }

    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'countdown-css', $plugin_url . 'css/countdown.css' );
    }

    /**
     * Load JS for plugin
     */
    public function load_plugin_js() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_script( 'countdown-js', $plugin_url . 'js/countdown.js', array('jquery') );
    }

    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
          array(
              'type'        => 'textfield',
              'heading'     => __( 'Title', 'fd-vc-countdown' ),
              'param_name'  => 'title',
              'description' => 'Used in the subnav',
          ),
          array(
              'type' => 'checkbox',
              'heading' => __('Sub menu', 'fd-vc-image'),
              'param_name' => 'display_submenu',
              'description' => __('Display in sub menu', 'fd-vc-image'),
          ),
          array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Target Time For Countdown", "fd-vc-countdown"),
            "param_name" => "datetime",
            "value" => "",
            "description" => __("Date and time format (yyyy-mm-dd hh:mm:ss).", "fd-vc-countdown"),
          ),
        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Countdown', 'fd-vc-countdown' ),
            'description' => __("Days, Hours, Minutes, Seconds to date",'fd-vc-countdown'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/icon-countdown.png', dirname( __FILE__ ) ),
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }


    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'datetime'        => '',
            'title'           => '',
            'display_submenu' => '',
        ) );

        /* ----------------
        // Set up random number to add to title ID and add title vaule to data attrubute
        ex Title ID = title
        */
        $remove = array(" ","'","&","(",")","[","]","{","}",".",":");
        $idSpace = mb_convert_case(str_replace(' ','-',$data['title']), MB_CASE_LOWER, "UTF-8");
        $id = mb_convert_case(str_replace($remove,'',$idSpace), MB_CASE_LOWER, "UTF-8");

        // submenu string
        $submenu = ' header__submenu--item" id="'.$id.'" data-title="'.$data['title'].'">';

        /* ----------------
        // Grab time diffrence between now and countdown date
        */
        $now = new DateTime();
        $datetime = new DateTime($data['datetime']);
        $interval = $now->diff($datetime);

        // convert to js datetime sting: yyyy-mm-ddThh:mm:ss => 2017-01-19T016:20:30
        $js_datetime = str_replace(" ", "T",$data['datetime']);
        // $js_datetime = '2019-01-19T16:20:30';

        // Start our output
        $output = '';
        // Start our section
        $output .= '<section data-datetime="'.$js_datetime.'" class="fd-vc-countdown '. ($data['display_submenu'] ? $submenu : '">');

        $output .='<div class="row d-
        flex justify-content-center">';
        if($interval->format('%Y') != 0){ // check if year diffrence is zero
          $output .='<div class="countdown__years countdown_unit text-center"><h2>'.$interval->format('%Y').'</h2><p>year</p></div>';
        }
        if($interval->format('%m') != 0){ // check if month diffrence is zero
          $output .='<div class="countdown__months countdown_unit text-center"><h2>'.$interval->format('%m').'</h2><p>Months</p></div>';
        }
        if($interval->format('%d') != 0){ // check if day diffrence is zero
          $output .='<div class="countdown__days countdown_unit text-center"><h2>'.$interval->format('%d').'</h2><p>Days</p></div>';
        }
        // if($interval->format('%H') > 13){
          $output .='<div class="countdown__hours countdown_unit text-center"><h2>'.$interval->format('%H').'</h2><p>Hours</p></div>';
        // } else{
        //   $output .='<div class="countdown__hours countdown_unit text-center"><h2>0</h2><p>Hours</p></div>';
        // }
        $output .='<div class="countdown__minutes countdown_unit text-center"><h2>'.$interval->format('%i').'</h2><p>Minutes</p></div>';
        $output .='<div class="countdown__seconds countdown_unit text-center"><h2>'.$interval->format('%s').'</h2><p>Seconds</p></div>';
        $output .='</div>';


        // Output the title if one exists
        // $output .= $data['datetime'] ? '<h2 class="section-title">' . esc_html( $data['datetime'] ) . '</h2>' : '';
        // Output the content if it exists
        $output .= $content ? apply_filters( 'the_content', $content ) : '';

        // Close our section
        $output .= '</section>';
        return $output;
    }
}
