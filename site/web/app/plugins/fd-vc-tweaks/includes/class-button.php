<?php
/**
 * FD Custom Content Element
 * @version 0.1.0
 * @package FD VC Tweaks
 */
class FDVC_Button {
    /**
     * Parent plugin class
     *
     * @var   class
     * @since 0.1.0
     */
    protected $plugin = null;
    /**
     * Constructor
     *
     * @since  0.1.0
     * @return void
     */
    public function __construct( $plugin ) {
        $this->plugin = $plugin;
        $this->hooks();
    }
    /**
     * Set the block name.
     */
    private $element_name = 'fdvc_button';
    public function hooks() {
        // Register (map) the new VC module
        add_action( 'vc_before_init', array( $this, 'vc_map' ) );
        // Register the block as a shortcode - Required to display!
        add_action( 'init', array( $this, 'register_shortcode' ) );
        // Register css
        add_action( 'init', array( $this, 'load_plugin_css' ) );
    }
    /**
     * Register a shortcode with WordPress.
     */
    public function register_shortcode() {
        add_shortcode( $this->element_name, array( $this, 'render_block' ) );
    }

    /**
     * Load CSS for plugin
     */
    public function load_plugin_css() {
        $plugin_url = plugin_dir_url( __FILE__ );
        wp_enqueue_style( 'button-css', $plugin_url . 'css/button.css' );
    }
    /**
     * Setup block defaults.
     */
    public function vc_map() {
        $fields = array(
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button Text','js_composer'),
                'param_name'  => 'button_text',
                'description' => '',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => __( 'Button URL','js_composer'),
                'param_name'  => 'button_url',
                'description' => 'Ex: "http://www.fieldays.co.nz" or "/home"',
            ),
             array(
                'type' => 'dropdown',
                'heading' => __('Link target','fd-vc-button'),
                'param_name' => 'link_target',
                'value' => array(
                  __('Same window','fd-vc-button')  => '_self',
                  __('New window','fd-vc-button')  => '_blank',
                ),
                'std'  => '_self',
                'description' => '',
              ),
            array(
                'type' => 'dropdown',
                'heading' => __('Button color','js_composer'),
                'param_name' => 'button_color',
                'value' => array(
                  __('Green','js_composer') => 'primary',
                  __('Grey','js_composer') => 'secondary',
                ),
                'description' => 'Green or grey button',
            ),
            array(
              "type" => "dropdown",
              "heading" => __("Alignment",'js_composer'),
              "param_name" => "button_alignment",
              "value" => array(
                __("Left","fd-vc-button") => 'button--left',
                __("Center","fd-vc-button") => 'button--center',
                __("Right","fd-vc-button") => 'button--right',
              ),
              'description' => 'Align left, center or right',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Alignment', 'js_composer'),
                'param_name' => 'align_tobox',
                'description' => 'Align to color text box',
            ),

        );
        // Block settings.
        $args = array(
            'base'     => $this->element_name,
            'name'     => __( 'Button', 'fd-vc-button' ),
            'description' => __('','fd-vc-button'),
            'class'    => $this->element_name,
            'category' => 'Fieldays',
            'params'   => $fields,
            'icon'     => plugins_url( 'assets/images/element-icon-button.svg', dirname( __FILE__ ) )
        );
        // Register block with Visual Composer.
        vc_map( $args );
    }
    /**
     * Setup shortcode attributes.
     */
    public function render_block( $atts, $content = null ) {
        $data = wp_parse_args( $atts, array(
            'button_url'          => '',
            'button_text'         => '',
            'button_color'        => '',
            'button_alignment'    => '',
            'align_tobox'         => '',
            'link_target'         => '',
        ) );

        $buttonColor = ($data['button_color'] === '' ? 'btn-primary' : 'btn-secondary' );
        $linkTarget = ($data['link_target'] === '' ? '_self' : '_blank' );

        // Build our button output
        $button_output = '';
        if ( $data['button_url'] && $data['button_text'] ) {
            $button_output = '<a href="' . esc_url( $data['button_url'] ) . '" class="btn '.$buttonColor.'" target= "'.$linkTarget.'">' . esc_html( $data['button_text'] ) . '</a>';
        }

        // Start our output
        $output = '';

        // Start our section
        $output .= '<section class="vc_button '.$data['button_alignment'].' '.($data['align_tobox'] ? 'align-to-colorboxtext' : '').'" >';
        $output .= $button_output;
        $output .= '</section>';
        return $output;


    }
}
