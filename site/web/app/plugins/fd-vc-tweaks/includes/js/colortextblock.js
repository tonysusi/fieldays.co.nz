jQuery(document).ready(function($) {

   // toggle height of color text block
  var readMore = $('.vc_color-text-block__btn a.btn-secondary');
  
  readMore.on("click touchstart", function(i){
      ($(this).parent().parent().hasClass('full-height') ) ?
      toggleHeight($(this).parent().parent(), $(this), 'remove'):
      toggleHeight($(this).parent().parent(), $(this), 'add');
  });

  function toggleHeight(el, btn, state){
    if(state === 'add'){
      el.addClass('full-height');
      btn.html('Read less');
    } else{
      el.removeClass('full-height');
      btn.html('Read more');
    }
  }

});
