jQuery(document).ready(function($) {       //wrapper

  var timer;
  var newdatetime = $('.fd-vc-countdown').attr('data-datetime')
  var compareDate = new Date(newdatetime);

  timer = setInterval(function() {
    timeBetweenDates(compareDate);
  }, 1000);

  function timeBetweenDates(toDate) {
    var dateEntered = toDate;
    var now = new Date();
    var difference = dateEntered.getTime() - now.getTime();
    if (difference <= 0) {
      // Timer done
      clearInterval(timer);
    } else {
      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var months = dateEntered.getMonth() - now.getMonth() + (12 * (dateEntered.getFullYear() - now.getFullYear()));
      var days = Math.floor((months*30) - (hours / 24));

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      months !==0 ? $(".fd-vc-countdown .countdown__months h2").text(months): $("#fd-vc-countdown .countdown__months").hide();
      days ==0 &&  months ==0 ?  $(".fd-vc-countdown .countdown__days").hide(): $("#fd-vc-countdown .countdown__days h2").text(days);
      hours ==0 && days ==0 &&  months ==0 ?  $(".fd-vc-countdown .countdown__hours").hide(): $("#fd-vc-countdown .countdown__hours h2").text(hours);
      $(".fd-vc-countdown .countdown__minutes h2").text(minutes);
      $(".fd-vc-countdown .countdown__seconds h2").text(seconds);

    }
  }
});
