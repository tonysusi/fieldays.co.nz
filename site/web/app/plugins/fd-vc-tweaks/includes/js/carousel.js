jQuery(document).ready(function($) { //wrapper

	function buildSubMenu() {
		// check if sub menu div is added by php
		if ($('.header__submenu').length) {
			$('.header__submenu--item').each(function(i, obj) {

				if (obj.dataset.title !== '' || obj.dataset.title !== undefined) {
					var title = obj.dataset.title.indexOf(':') > -1 ? obj.dataset.title.replace(':','') : obj.dataset.title;
					var subMenuItem = '<li><a href="#' + obj.id + '">' + title + '</a></li>';
					$('.header__submenu ul').append(subMenuItem);
				}

			});
			smoothScroll();
		}
	}

	function smoothScroll() {
		// Add smooth scrolling to all links
		$(".header__submenu ul.nav li a").on('click', function(event) {

			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
				// Prevent default anchor click behavior
				event.preventDefault();

				// Store hash
				var hash = this.hash;

				// Using jQuery's animate() method to add smooth page scroll
				// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 800, function() {

					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				});
			} // End if
		});
	}
	buildSubMenu();

});
