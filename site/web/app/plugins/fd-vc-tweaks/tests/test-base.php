<?php
/**
 * WDS_VC_Tweaks.
 *
 * @since   0.1.0
 * @package WDS_VC_Tweaks
 */
class WDS_VC_Tweaks_Test extends WP_UnitTestCase {

	/**
	 * Test if our class exists.
	 *
	 * @since  0.1.0
	 */
	function test_class_exists() {
		$this->assertTrue( class_exists( 'WDS_VC_Tweaks') );
	}

	/**
	 * Test that our main helper function is an instance of our class.
	 *
	 * @since  0.1.0
	 */
	function test_get_instance() {
		$this->assertInstanceOf(  'WDS_VC_Tweaks', wds_vs_tweaks() );
	}

	/**
	 * Replace this with some actual testing code.
	 *
	 * @since  0.1.0
	 */
	function test_sample() {
		$this->assertTrue( true );
	}
}
