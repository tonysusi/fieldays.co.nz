# WDS Visual Composer Tweaks for Fieldays #
**Contributors:**      Tony Susi  
**Donate link:**       http://fieldays.co.nz  
**Tags:**  
**Requires at least:** 4.4  
**Tested up to:**      4.8.1 
**Stable tag:**        0.1.0  
**License:**           GPLv2  
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html  

## Description ##

Tweaks for WPBakery / VC for the Fieldays Theme

## Installation ##

### Manual Installation ###

1. Upload the entire `/wds-vs-tweaks` directory to the `/wp-content/plugins/` directory.
2. Activate WDS Visual Composer Tweaks for Fieldays through the 'Plugins' menu in WordPress.

## Frequently Asked Questions ##


## Screenshots ##


## Changelog ##

### 0.1.0 ###
* First release

## Upgrade Notice ##

### 0.1.0 ###
First Release
