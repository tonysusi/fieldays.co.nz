<?php
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"'); 

if(isset($_COOKIE['PHPSESSID'])){
  ini_set("session.use_trans_sid",0);
}else{
  ini_set("session.use_trans_sid",1);
}

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors',1);
ini_set('html_errors', 1);

require_once('system/vendor/autoload.php');

// depedency injection container
$c = require('system/include/services.php');

// sane runtime environment
error_reporting($c['config']['php.error_reporting']);
ini_set('display_errors', $c['config']['php.display_errors']);
ini_set('log_errors', $c['config']['php.log_errors']);
ini_set('error_log', $c['config']['php.error_log']);


session_cache_limiter(false);
session_start();

// load routes and run application
$app = $c['app'];
require_once($config['path.routes'] . '_main.php');

ob_start("sanitize_output");
$app->run();
ob_end_flush();